import { filter as _filter } from 'lodash-es'
import Model from '@/generators/model.generator'
import IdentityMapping from '../mapping/identity.mapping'
import TimeslotMapping from '../mapping/timeslot.mapping'

class Room extends Model {
  constructor (id) {
    super(id)

    // Submodel declaration
    this._submodelConfig = {
      identity: {
        mapping: IdentityMapping,
        isMock: true,
        url: `rooms/room_${id}`
      },
      timeslots: {
        mapping: TimeslotMapping,
        isCollection: true,
        isMock: true,
        url: `timeslots/room_${id}_timeslots`
      }
    }
  }

  /**
   * Update identity status, this is usualy done
   * on request update. But models give us the possibility
   * to update the status directly on user actions. It can be usefull
   * to increase app preformance (less requests) or for mocks
   */
  updateIdentityStatus () {
    if (!this.identity || !this.timeslots) {
      return
    }

    const availableTimeSlots = _filter(this.timeslots, { booked: false })
    if (availableTimeSlots.length && availableTimeSlots.length < 5) {
      this.identity.states[0].active = false
      this.identity.states[1].active = true
    }

    if (availableTimeSlots.length === 0) {
      this.identity.states[0].active = true
      this.identity.states[1].active = false
    }

    this.identity.messages.availability.message = this.identity._getAvailabilityMessage()
    this.identity.messages.equipment.active = this.identity._checkEquipmentMessageState()
  }
}

export default Object.seal(Room)
