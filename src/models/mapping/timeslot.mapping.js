import { get as _get } from 'lodash-es'

class Timeslot {
  constructor (data) {
    this.set(data)
  }

  /**
   * Populate room identity model
   * @param {object} data (identity)
   */
  set (data) {
    this.booked = _get(data, 'is_booked', false)
    this.link = {
      userId: _get(data, 'user_id', null),
      roomId: _get(data, 'room_id', null)
    }
    this.timeslot = {
      meridiem: _get(data, 'meridiem'),
      start: _get(data, 'hour_start'),
      end: _get(data, 'hour_stop')
    }
  }
}

export default Object.seal(Timeslot)
