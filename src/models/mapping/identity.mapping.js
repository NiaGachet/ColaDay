import {
  get as _get,
  find as _find
} from 'lodash-es'

class Identity {
  constructor (data) {
    this.set(data)
  }

  /**
   * Populate room identity model
   * @param {object} data (identity)
   */
  set (data) {
    this.id = _get(data, 'id', null)
    this.label = _get(data, 'title', null)
    this.picture = _get(data, 'image_url', null)
    this.organizationTitle = _get(data, 'organization', null)
    this.specifications = {
      seats: {
        id: 'seats',
        number: _get(data, 'seats', 0)
      },
      options: [
        {
          id: 'conf-call',
          available: this._isAvailable(data, 'options.conf_call'),
          issue: this._isOutOfService(data, 'options.conf_call')
        },
        {
          id: 'tv-projector',
          available: this._isAvailable(data, 'options.tv_projector'),
          issue: this._isOutOfService(data, 'options.tv_projector')
        },
        {
          id: 'water-fountain',
          available: this._isAvailable(data, 'options.water_fountain'),
          issue: this._isOutOfService(data, 'options.water_fountain')
        },
        {
          id: 'coffee-machin',
          available: this._isAvailable(data, 'options.coffee_machin'),
          issue: this._isOutOfService(data, 'options.coffee_machin')
        },
        {
          id: 'air-conditioned',
          available: this._isAvailable(data, 'options.air_conditioned'),
          issue: this._isOutOfService(data, 'options.air_conditioned')
        },
        {
          id: 'whiteboard',
          available: this._isAvailable(data, 'options.whiteboard'),
          issue: this._isOutOfService(data, 'options.whiteboard')
        }
      ]
    }
    this.states = [
      {
        id: 'not-available',
        active: _get(data, 'state', null) === 'not_available'
      },
      {
        id: 'limited-timeslots',
        active: _get(data, 'state', null) === 'limited_timeslots'
      }
    ]
    this.messages = {
      equipment: {
        message: 'Some equipment need to be reviewed!',
        active: this._checkEquipmentMessageState()
      },
      availability: {
        message: this._getAvailabilityMessage()
      }
    }
  }

  /**
   * Check if option is available
   * @param  {Object}  data (row data)
   * @param  {String}  path (path to option)
   *
   * @return {Boolean}
   */
  _isAvailable (data, path) {
    return _get(data, path) === 'ooservice' || _get(data, path) === 'enable'
  }

  /**
   * Check if option is out of service
   * @param  {Object}  data (row data)
   * @param  {String}  path (path to option)
   * @return {Boolean}
   */
  _isOutOfService (data, path) {
    return _get(data, path) === 'ooservice'
  }

  /**
   * Generate Equipment messages
   */
  _checkEquipmentMessageState () {
    const issue = _find(this.specifications.options, option => {
      return option.issue && option.available
    })

    return Boolean(issue)
  }

  /**
   * Generate availability messages
   */
  _getAvailabilityMessage () {
    const activeState = _find(this.states, 'active')

    switch (_get(activeState, 'id')) {
      case 'not-available':
        return 'Meeting room not available!'
      case 'limited-timeslots':
        return 'Meeting room with limited availabilities!'
      default:
        return 'Meeting room available.'
    }
  }
}

export default Object.seal(Identity)
