import { get as _get } from 'lodash-es'

class User {
  constructor (data) {
    this.set(data)
  }

  /**
   * Populate room identity model
   * @param {object} data (identity)
   */
  set (data) {
    this.id = _get(data, 'id', null)
    this.firstName = _get(data, 'firstname', null)
    this.lastname = _get(data, 'lastname', null)
    this.fullName = `${this.firstName} ${this.lastname}`
  }
}

export default Object.seal(User)
