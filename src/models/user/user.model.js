import Model from '@/generators/model.generator'
import UserProfile from '../mapping/profile.mapping'
import TimeslotMapping from '../mapping/timeslot.mapping'

class User extends Model {
  constructor (id, username, password) {
    super(id)

    // Submodel declaration
    this._submodelConfig = {
      profile: {
        mapping: UserProfile,
        isMock: true,
        url: `users/user_${username}_${password}`
      },
      timeslots: {
        mapping: TimeslotMapping,
        isCollection: true,
        isMock: true
      }
    }
  }
}

export default Object.seal(User)
