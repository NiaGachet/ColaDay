import '@babel/polyfill'
import 'mdi/css/materialdesignicons.min.css'
import Vue from 'vue'
import './plugins/vuetify'
import App from './components/app/App.vue'
import router from './router'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
