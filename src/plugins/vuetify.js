import Vue from 'vue'
import {
  Vuetify,
  VApp,
  VSwitch,
  VMenu,
  VTextarea,
  VNavigationDrawer,
  VExpansionPanel,
  VProgressCircular,
  VSubheader,
  VSnackbar,
  VAvatar,
  VChip,
  VList,
  VCard,
  VBtn,
  VIcon,
  VGrid,
  VDivider,
  VForm,
  VSelect,
  VCheckbox,
  VTextField,
  VTooltip,
  VToolbar,
  transitions
} from 'vuetify'
import 'vuetify/src/stylus/app.styl'

const buildTarget = process.env.VUE_APP_BUILD_TARGET
let theme

if (buildTarget === 'coca') {
  theme = {
    primary: '#ed1c16',
    secondary: '#212121',
    accent: '#f5f5f5',
    error: '#ff5252',
    info: '#ed1c16',
    success: '#4caf50'
  }
} else {
  theme = {
    primary: '#004883',
    secondary: '#e32934',
    accent: '#f5f5f5',
    error: '#ff5252',
    info: '#2196f3',
    success: '#4caf50'
  }
}

Vue.use(Vuetify, {
  components: {
    VApp,
    VSwitch,
    VNavigationDrawer,
    VMenu,
    VProgressCircular,
    VExpansionPanel,
    VSubheader,
    VTextarea,
    VSnackbar,
    VTextField,
    VCheckbox,
    VAvatar,
    VTooltip,
    VForm,
    VSelect,
    VChip,
    VList,
    VDivider,
    VBtn,
    VIcon,
    VGrid,
    VCard,
    VToolbar,
    transitions
  },
  theme
})
