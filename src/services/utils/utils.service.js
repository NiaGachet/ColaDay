import {
  isString as _isString,
  trim as _trim,
  map as _map
} from 'lodash-es'

class utils {
  /**
   * Convert word, word, word to ['word', 'word', 'word']
   * and Trim array values
   * @param  {String/Array} values
   *
   * @return {Array}
   */
  static stringToArray (values) {
    return _isString(values)
      ? _map(values.split(','), _trim)
      : _map(values, _trim)
  }

  /**
   * Counter
   * @param  {Object} timer (with duration and instance)
   *
   * @return {Object}
   */
  static counter (timer, callback) {
    timer.instance = setInterval(() => {
      timer.duration--
      if (timer.duration === 0) {
        callback()
        clearInterval(timer.instance)
      }
    }, 1000)

    return timer
  }
}

export default utils
