class states {
  /**
   * Get state color of an equipment icon
   * @param  {Object} option (specification option)
   *
   * @return {String}        (class color)
   */
  static getEquipmentColor (option) {
    return option.issue ? 'error' : 'success'
  }
}

export default states
