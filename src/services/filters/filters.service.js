import {
  isUndefined as _isUndefined,
  filter as _filter,
  find as _find
} from 'lodash-es'

class filters {
  /**
   * Count consecutive timeslot available
   *
   * @param  {Array}   timeslots  (Time slots model data)
   * @param  {Integer} startIndex (Starting hour index, optional)
   *
   * @return {Integer}            (consecutive available slots)
   */
  static consecutiveTimeslotCounter (timeslots, startIndex) {
    const counters = []

    if (startIndex) {
      counters[0] = 0
      if (startIndex === -1) return counters[0]
      for (let i = startIndex; i < timeslots.length; i++) {
        counters[0]++
        if (timeslots[i].booked) {
          counters[0]--
          break
        }
      }
      return counters[0]
    }

    let range = 0
    for (let i = 0; i < timeslots.length; i++) {
      if (_isUndefined(counters[range])) counters[range] = 0
      counters[range]++
      if (timeslots[i].booked) {
        counters[range]--
        range++
      }
    }

    return Math.max(...counters)
  }

  /**
   * Filter equipment by checking options
   * inside a meeting room model
   * @param  {Array} data (Model collection)
   * @param  {Strong} id  (option id)
   *
   * @return {Boolean}
   */
  static filterEquipment (data, id) {
    return _filter(data, item => {
      const option = _find(item.identity.specifications.options, { id })
      return option.available && !option.issue
    })
  }
}

export default filters
