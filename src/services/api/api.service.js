class api {
  /**
   * Avoid setTimeout error-handling characteristics
   * @param  {Integer} ms (duration)
   *
   * @return {Promise}
   */
  static wait (ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
  }

  /**
   * Fake update with payload
   * @param  {String} id   (resource id)
   * @param  {Object} data (payload)
   * @param  {String} desc (description)
   *
   * @return {Promise}
   */
  static async updateWithPayload (id, data, desc) {
    await api.wait(1000)
    console.info(`${desc} with id ${id} has been updated with`, data)
  }

  /**
   * Fake update without payload
   * @param  {String} id   (request id)
   * @param  {String} desc (description)
   *
   * @return {Promise}
   */
  static async updateWithoutPayload (id, desc) {
    await api.wait(1000)
    console.info(`${desc} with id ${id} updated`)
  }
}

export default api
