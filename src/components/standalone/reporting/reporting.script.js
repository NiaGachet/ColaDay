import statesService from '@/services/states/states.service'
import Component from '@/generators/component.generator'
import api from  '@/services/api/api.service'
import meta from './meta.json'

// Component name
const name = 'reporting'

// Component declaration
const component = new Component(name)

// Props
const properties = [
  'value'
];

/**
 * Send message
 */
function sendMessage () {
  const obj = {
    message: this.meta.message.value
  }
  this.meta.message.states.loading = true
  api
    .updateWithPayload(this.value.id, obj, 'Report message')
    .then(() => {
      this.meta.message.states.loading = false
      this.meta.message.states.active = true
    })
}

/**
 * Get state color of an equipment
 * @param  {Object} option (specification option)
 *
 * @return {String}        (class color)
 */
function getEquipmentStateColor(option) {
  return statesService.getEquipmentColor(option)
}

/**
 * Get human readable status text
 * @param  {Boolean} issueState (has issue)
 *
 * @return {String}
 */
function getStatusReadable (issueState) {
  return issueState ? 'Out of service' : 'Nothing to report to date'
}

// Data
const ModelObject = {
  meta
}

component
  .setModel(ModelObject)
  .setMethods(sendMessage)
  .setMethods(getStatusReadable)
  .setMethods(getEquipmentStateColor)
  .setBoundProperties(properties)

export default component
