import { mount, createLocalVue } from '@vue/test-utils'
import c01Mock from '@/assets/mocks/rooms/room_c01'
import RoomModel from '@/models/room/room.model'
import Reporting from './Reporting'
import Vuetify from 'vuetify'

const selectors = {}
selectors.reporting = '.reporting-component'
selectors.reportingEquipmentList = `${selectors.reporting} .v-expansion-panel > li`
selectors.reportingTextArea = `${selectors.reporting} .v-textarea`

describe('Reporting component', () => {
  let wrapper

  const localVue = createLocalVue()
  localVue.use(Vuetify)

  const meetingRoom = new RoomModel('c01')
  meetingRoom.setSubModel({
    name: 'identity',
    data: c01Mock.response
  })

  beforeEach(() => {
    wrapper = mount(Reporting, {
      localVue,
      propsData: {
        value: meetingRoom.getSubModel('identity')
      }
    })
  })

  it('Should exist', () => {
    expect(wrapper.find(selectors.reporting).exists()).toBe(true)
  })

  it('Should have list with 4 items', () => {
    expect(wrapper.findAll(selectors.reportingEquipmentList).length).toBe(4)
  })

  it('Should have first equipment deficient', () => {
    expect(wrapper.findAll(selectors.reportingEquipmentList).at(0).find('.v-icon').classes()).toContain('error--text')
  })

  it('Should have second equipment on service', () => {
    expect(wrapper.findAll(selectors.reportingEquipmentList).at(1).find('.v-icon').classes()).toContain('success--text')
  })

  it('Should have third equipment on service', () => {
    expect(wrapper.findAll(selectors.reportingEquipmentList).at(2).find('.v-icon').classes()).toContain('success--text')
  })

  it('Should have last equipment on service', () => {
    expect(wrapper.findAll(selectors.reportingEquipmentList).at(3).find('.v-icon').classes()).toContain('success--text')
  })

  it('Should have a message area', () => {
    expect(wrapper.find(selectors.reportingTextArea).exists()).toBe(true)
  })
})
