import meta from './meta.json'
import { clone as _clone } from 'lodash-es'
import api from  '@/services/api/api.service'
import utils from '@/services/utils/utils.service'
import Component from '@/generators/component.generator'

// Component name
const name = 'timeslots'

// Component declaration
const component = new Component(name)

// Props
const properties = [
  'value',
  'user'
];

// Data
const ModelObject = {
  meta
}

/**
 * Get color for user booking
 * @param  {Strong} id (User id of the time slot)
 *
 * @return {String}
 */
function getColor (id) {
  return this.user.profile.id === id ? 'error' : ''
}

/**
 * Pre-booking request
 * @param  {Object} slot   (time slot)
 * @param  {Integer} index (Index)
 */
function preBooking(slot, index) {
  this.$set(meta.states.buttons[index], 'loading', true)

  api
    .updateWithPayload(slot.link.roomId, slot.timeslot, 'Timeslot prebooking')
    .then(() => {
      this.meta.states.timer.duration = 15
      utils.counter(this.meta.states.timer, () => {
        this.cancelBooking(slot, index)
      })
      this.meta.states.preBooking = true
      this.$set(meta.states.buttons[index], 'preBooking', true)
      this.$set(meta.states.buttons[index], 'loading', false)
      this.meta.states.snackbar.active = true
    })
}

/**
 * Booking confirmation request
 * @param  {Object} slot  (Time slot)
 * @param  {Integer} index (index)
 */
function confirmBooking(slot, index) {
  clearInterval(this.meta.states.timer.instance)
  this.$set(meta.states.buttons[index], 'loading', true)
  this.$set(meta.states.buttons[index], 'cancelPre', false)

  api
    .updateWithPayload(slot.link.roomId, slot.timeslot, 'Timeslot booking')
    .then(() => {
      slot.booked = true
      slot.link.userId = this.user.profile.id
      this.user.timeslots[index] = slot
      this.$set(meta.states.buttons[index], 'loading', false)
      this.$set(meta.states.buttons[index], 'preBooking', false)
      this.meta.states.snackbar.active = false
      this.$set(meta.states.buttons[index], 'cancelPre', true)
      this.meta.states.preBooking = false
      this.$emit('updateIdentityStatus', slot.link.roomId)
    })
}

/**
 * Booking cancel request
 * @param  {Object}  slot  (Time slot)
 * @param  {Integer} index (Index)
 * @param  {String}  id    (Specific room id)
 */
function cancelBooking(slot, index, id) {
  clearInterval(this.meta.states.timer.instance)
  this.$set(meta.states.buttons[index], 'confirmPre', false)
  this.$set(meta.states.buttons[index], 'loading', true)
  const roomId = id || slot.link.roomId

  api
    .updateWithoutPayload(roomId, 'Timeslot booking')
    .then(() => {
      if (this.user) {
        this.user.timeslots[index].booked = false
      }
      if (!this.user) {
        slot.booked = false
      }
      this.$set(meta.states.buttons[index], 'loading', false)
      this.$set(meta.states.buttons[index], 'preBooking', false)
      this.meta.states.snackbar.active = false
      this.$set(meta.states.buttons[index], 'confirmPre', true)
      this.meta.states.preBooking = false
    })
}

/**
 * Generate meta by adding states for each time slot button
 * @param  {Integer} index (array index)
 */
function generateBookButtonsStates(index) {
  if (!this.value) {
    console.warn('Timeslots Component: v-model should not be empty')
    return
  }
  const buttonStates = [];
  const buttonState = {
    loading: false,
    preBooking: false,
    cancelPre: true,
    confirmPre: true
  }
  this.value.forEach(timeslot => {
    buttonStates.push(_clone(buttonState))
  })

  this.$set(this.meta.states, 'buttons', buttonStates)
}

component
  .setModel(ModelObject)
  .setMethods(getColor)
  .setMethods(preBooking)
  .setMethods(cancelBooking)
  .setMethods(confirmBooking)
  .setBoundProperties(properties)
  .setBeforeMount(generateBookButtonsStates)

export default component
