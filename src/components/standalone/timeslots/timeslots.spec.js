import { mount, createLocalVue } from '@vue/test-utils';
import c01Mock from '@/assets/mocks/timeslots/room_c01_timeslots'
import userTSmock from '@/assets/mocks/timeslots/user_agachet_timeslots'
import userProfileMock from '@/assets/mocks/users/user_anthony_123456'
import RoomModel from '@/models/room/room.model'
import UserModel from '@/models/user/user.model'
import Timeslots from './Timeslots'
import Vuetify from 'vuetify'

const selectors = {}
selectors.timeslots = '.timeslots-component'
selectors.timeslot = `${selectors.timeslots} .timeslots__container`

describe('Timeslots component', () => {
  let wrapper

  const localVue = createLocalVue()
  localVue.use(Vuetify)

  const meetingRoom = new RoomModel('c01')
  meetingRoom.setSubModel({
    name: 'timeslots',
    data: c01Mock.response
  })

  const user = new UserModel('me', 'anthony', '123456')
  user
    .setSubModel({ name: 'profile', data: userProfileMock.response })
    .setSubModel({ name: 'timeslots', data: userTSmock.response })

  beforeEach(() => {
    wrapper = mount(Timeslots, {
      localVue,
      propsData: {
        value: meetingRoom.getSubModel('timeslots'),
        user
      }
    })
  })

  it('Should exist', () => {
    expect(wrapper.find(selectors.timeslots).exists()).toBe(true)
  })

  it('should display list of timeslots equal to existing timeslots', () => {
    expect(wrapper.findAll(selectors.timeslot).length).toBe(meetingRoom.timeslots.length)
  })

  it('should have timeslots starting by 8am', () => {
    expect(wrapper.findAll(selectors.timeslot).at(0).text()).toMatch(/8am/)
  })

  it('should have timeslots finishing by 6pm', () => {
    expect(wrapper.findAll(selectors.timeslot).at(9).text()).toMatch(/6pm/)
  })

  it('should have timeslot 8am to 9am booked and disabled', () => {
    expect(wrapper.findAll(selectors.timeslot).at(0).text()).toMatch(/Booked/)
    expect(wrapper.findAll(selectors.timeslot).at(0).find('button').attributes().disabled).toBe('disabled')
  })

  it('should have timeslot 9am to 10am available and enabled', () => {
    expect(wrapper.findAll(selectors.timeslot).at(1).text()).toMatch(/Book/)
    expect(wrapper.findAll(selectors.timeslot).at(1).find('button').attributes().disabled).not.toBe('disabled')
  })

  it('should have the timeslot 10am to 11am available and enebled', () => {
    expect(wrapper.findAll(selectors.timeslot).at(2).text()).toMatch(/Book/)
    expect(wrapper.findAll(selectors.timeslot).at(2).find('button').attributes().disabled).not.toBe('disabled')
  })

  it('should have the timeslot 11am to 12am booked and disabled', () => {
    expect(wrapper.findAll(selectors.timeslot).at(3).text()).toMatch(/Booked/)
    expect(wrapper.findAll(selectors.timeslot).at(3).find('button').attributes().disabled).toBe('disabled')
  })

  it('should have the timeslot 12am to 1pm booked and disabled', () => {
    expect(wrapper.findAll(selectors.timeslot).at(4).text()).toMatch(/Booked/)
    expect(wrapper.findAll(selectors.timeslot).at(4).find('button').attributes().disabled).toBe('disabled')
  })

  it('should have the timeslot 1pm to 2pm available and enabled', () => {
    expect(wrapper.findAll(selectors.timeslot).at(5).text()).toMatch(/Book/)
    expect(wrapper.findAll(selectors.timeslot).at(5).find('button').attributes().disabled).not.toBe('disabled')
  })

  it('should have the timeslot 2pm to 3pm available and enabled', () => {
    expect(wrapper.findAll(selectors.timeslot).at(6).text()).toMatch(/Book/)
    expect(wrapper.findAll(selectors.timeslot).at(6).find('button').attributes().disabled).not.toBe('disabled')
  })

  it('should have the timeslot 3pm to 4pm booked and disabled', () => {
    expect(wrapper.findAll(selectors.timeslot).at(7).text()).toMatch(/Booked/)
    expect(wrapper.findAll(selectors.timeslot).at(7).find('button').attributes().disabled).toBe('disabled')
  })

  it('should have the timeslot 4pm to 5pm booked and disabled', () => {
    expect(wrapper.findAll(selectors.timeslot).at(8).text()).toMatch(/Booked/)
    expect(wrapper.findAll(selectors.timeslot).at(8).find('button').attributes().disabled).toBe('disabled')
  })

  it('should have the timeslot 5pm to 6pm booked and disabled', () => {
    expect(wrapper.findAll(selectors.timeslot).at(9).text()).toMatch(/Booked/)
    expect(wrapper.findAll(selectors.timeslot).at(9).find('button').attributes().disabled).toBe('disabled')
  })
})


