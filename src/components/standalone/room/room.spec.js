import { mount, createLocalVue } from '@vue/test-utils'
import c01Mock from '@/assets/mocks/rooms/room_c01'
import RoomModel from '@/models/room/room.model'
import Room from './Room'
import Vuetify from 'vuetify'

const selectors = {}
selectors.room = '.room-component'
selectors.media = `${selectors.room} .v-card__media`
selectors.mediaPicture = `${selectors.media} .v-card__media__background`
selectors.mediaTitle = `${selectors.media} .media__title`
selectors.indicators = `${selectors.room} .indicators`
selectors.indicatorsAttendees = `${selectors.indicators} .v-chip__content > span`
selectors.indicatorsPhone = `${selectors.indicators} .mdi-phone`
selectors.indicatorsProjector = `${selectors.indicators} .mdi-television`
selectors.indicatorsWater = `${selectors.indicators} .mdi-cup-water`
selectors.indicatorsAirConditioned = `${selectors.indicators} .mdi-thermometer-lines`
selectors.indicatorsWhiteboard = `${selectors.indicators} .mdi-pencil`
selectors.indicatorsLimited = `${selectors.indicators} .mdi-clock-alert`
selectors.indicatorsNotAvailable = `${selectors.indicators} .mdi-cancel`
selectors.messages = `${selectors.room} .messages`
selectors.actions = `${selectors.room} .v-card__actions`
selectors.actionsReport = `${selectors.actions} .report`
selectors.actionsBook = `${selectors.actions} .book`

describe('Room component', () => {
  let wrapper

  const localVue = createLocalVue()
  localVue.use(Vuetify)

  const meetingRoom = new RoomModel('c01')
  meetingRoom.setSubModel({
    name: 'identity',
    data: c01Mock.response
  })

  beforeEach(() => {
    wrapper = mount(Room, {
      localVue,
      propsData: {
        value: meetingRoom.getSubModel('identity')
      }
    })
  })

  it('Should exist', () => {
    expect(wrapper.find(selectors.room).exists()).toBe(true)
  })

  it('Should have media block displayed', () => {
    expect(wrapper.find(selectors.media).exists()).toBe(true)
  })

  it('Should have picture loaded in media block', () => {
    expect(wrapper.find(selectors.mediaPicture).html()).toMatch(/card__media__background/)
  })

  it('Should have correct title loaded in media block', () => {
    expect(wrapper.find(selectors.mediaTitle).text()).toMatch(meetingRoom.identity.label)
  })

  it('Should have correct organization loaded in media block', () => {
    expect(wrapper.find(selectors.mediaTitle).text()).toMatch(meetingRoom.identity.organizationTitle)
  })

  it('Should have equipment indicators panel displayed', () => {
    expect(wrapper.find(selectors.indicators).exists()).toBe(true)
  })

  it('Should have attendees indicator with correct attendees number', () => {
    const attendeesNumber = meetingRoom.identity.specifications.seats.number
    expect(Number(wrapper.find(selectors.indicatorsAttendees).text())).toBe(attendeesNumber)
  })

  it('Should have conferencing indicator not displayed', () => {
    expect(wrapper.find(selectors.indicatorsPhone).exists()).toBe(false)
  })

  it('Should have "Projector" indicator displayed with "out of service" state', () => {
    expect(wrapper.find(selectors.indicatorsProjector).classes()).toContain('error--text')
  })

  it('Should have "Water" indicator displayed with "available" state', () => {
    expect(wrapper.find(selectors.indicatorsWater).classes()).toContain('success--text')
  })

  it('Should have "Air conditioned" indicator displayed with "available" state', () => {
    expect(wrapper.find(selectors.indicatorsAirConditioned).classes()).toContain('success--text')
  })

  it('Should have "Whiteboard" indicator displayed with "available" state', () => {
    expect(wrapper.find(selectors.indicatorsWhiteboard).classes()).toContain('success--text')
  })

  it('Should have "Limited time slots" indicator displayed', () => {
    expect(wrapper.find(selectors.indicatorsLimited).exists()).toBe(true)
  })

  it('Should not have "No time slots available" indicator displayed', () => {
    expect(wrapper.find(selectors.indicatorsNotAvailable).exists()).toBe(false)
  })

  it('Should display an availability message', () => {
    const message = meetingRoom.identity.messages.availability.message
    expect(wrapper.find(selectors.messages).text()).toMatch(message)
  })

  it('Should display an equipment error message', () => {
    const message = meetingRoom.identity.messages.equipment.message
    expect(wrapper.find(selectors.messages).text()).toMatch(message)
  })

  it('Should have actions block displayed', () => {
    expect(wrapper.find(selectors.actions).exists()).toBe(true)
  })

  it('Should have "Make a Report" action button displayed', () => {
    expect(wrapper.find(selectors.actionsReport).exists()).toBe(true)
  })

  it('Should have "Book" action button displayed', () => {
    expect(wrapper.find(selectors.actionsBook).exists()).toBe(true)
  })
})
