import Component from '@/generators/component.generator'
import statesService from '@/services/states/states.service'
import meta from './meta.json'

// Component name
const name = 'meetingRoom'

// Component declaration
const component = new Component(name)

// Props
const properties = [
  'value'
];

// Data
const ModelObject = {
  meta
}

/**
 * Create image url to inject
 * @param  {String} img (image name)
 *
 * @return {String}     (image URL)
 */
function getImgUrl(img) {
  return require('@/assets/rooms-pictures/' + img)
}

/**
 * Get state color of an equipment icon
 * @param  {Object} option (specification option)
 *
 * @return {String}        (class color)
 */
function getEquipmentStateColor(option) {
  return statesService.getEquipmentColor(option)
}

component
  .setModel(ModelObject)
  .setMethods(getImgUrl)
  .setMethods(getEquipmentStateColor)
  .setBoundProperties(properties)

export default component
