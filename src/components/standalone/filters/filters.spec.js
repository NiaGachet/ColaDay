import { mount, createLocalVue } from '@vue/test-utils';
import Filter from './Filters'
import Vuetify from 'vuetify'

const selectors = {}
selectors.filters = '.filters-component'
selectors.startingHourFilter = `${selectors.filters} .starting-hour`
selectors.startingHourFilterList = `${selectors.startingHourFilter} .v-list > div`
selectors.durationFilter = `${selectors.filters} .duration`
selectors.durationFilterList = `${selectors.durationFilter} .v-list > div`
selectors.minimumSeatsFilter = `${selectors.filters} .minimum-seats`
selectors.minimumSeatsFilterInput = `${selectors.minimumSeatsFilter} input`
selectors.equipmentFilters = `${selectors.filters} .equipment`
selectors.equipmentFilterConferencing = `${selectors.equipmentFilters} .conferencing input`
selectors.equipmentFilterProjector = `${selectors.equipmentFilters} .projector input`
selectors.equipmentFilterWater = `${selectors.equipmentFilters} .water input`
selectors.equipmentFilterCoffee = `${selectors.equipmentFilters} .coffee input`
selectors.equipmentFilterAirConditioned = `${selectors.equipmentFilters} .air-conditioned input`
selectors.equipmentFilterWhiteboard = `${selectors.equipmentFilters} .whiteboard input`

describe('Filter component', () => {
  let wrapper

  const localVue = createLocalVue()
  localVue.use(Vuetify)

  beforeEach(() => {
    wrapper = mount(Filter, {
      localVue
    })
  })

  it('Should exist', () => {
    expect(wrapper.find(selectors.filters).exists()).toBe(true)
  })

  it('Should have starting hour filter', () => {
    expect(wrapper.find(selectors.startingHourFilter).exists()).toBe(true)
  })

  it('Should have starting hour list with 10 items', () => {
    expect(wrapper.findAll(selectors.startingHourFilterList).length).toBe(10)
  })

  it('Should have starting hour list starting from 8am', () => {
    expect(wrapper.findAll(selectors.startingHourFilterList).at(0).text()).toBe('8am')
  })

  it('Should have starting hour list ending with 5pm', () => {
    expect(wrapper.findAll(selectors.startingHourFilterList).at(9).text()).toBe('5pm')
  })

  it('Should have duration filter', () => {
    expect(wrapper.find(selectors.durationFilter).exists()).toBe(true)
  })

  it('Should have duration list with 10 items', () => {
    expect(wrapper.findAll(selectors.durationFilterList).length).toBe(10)
  })

  it('Should have starting hour list starting from 1h', () => {
    expect(wrapper.findAll(selectors.durationFilterList).at(0).text()).toBe('1h')
  })

  it('Should have starting hour list ending with 10h', () => {
    expect(wrapper.findAll(selectors.durationFilterList).at(9).text()).toBe('10h')
  })

  it('Should have minimum seats filter', () => {
    expect(wrapper.find(selectors.minimumSeatsFilter).exists()).toBe(true)
  })

  it('Should have minimum seats filter with default value 2', () => {
    expect(wrapper.find(selectors.minimumSeatsFilterInput).element.value).toBe('2')
  })

  it('Should have equipment filters', () => {
    expect(wrapper.find(selectors.equipmentFilters).exists()).toBe(true)
  })

  it('Should have conferencing equipment filter existing and not checked', () => {
    expect(wrapper.find(selectors.equipmentFilterConferencing).attributes()['aria-checked']).toBe('false')
    wrapper.find(selectors.equipmentFilterConferencing).trigger('click')
    expect(wrapper.find(selectors.equipmentFilterConferencing).attributes()['aria-checked']).toBe('true')
  })

  it('Should have projector equipment filter existing and not checked', () => {
    expect(wrapper.find(selectors.equipmentFilterProjector).attributes()['aria-checked']).toBe('false')
    wrapper.find(selectors.equipmentFilterProjector).trigger('click')
    expect(wrapper.find(selectors.equipmentFilterProjector).attributes()['aria-checked']).toBe('true')
  })

  it('Should have water fountain equipment filter existing and not checked', () => {
    expect(wrapper.find(selectors.equipmentFilterWater).attributes()['aria-checked']).toBe('false')
    wrapper.find(selectors.equipmentFilterWater).trigger('click')
    expect(wrapper.find(selectors.equipmentFilterWater).attributes()['aria-checked']).toBe('true')
  })

  it('Should have coffee machin equipment filter existing and not checked', () => {
    expect(wrapper.find(selectors.equipmentFilterCoffee).attributes()['aria-checked']).toBe('false')
    wrapper.find(selectors.equipmentFilterCoffee).trigger('click')
    expect(wrapper.find(selectors.equipmentFilterCoffee).attributes()['aria-checked']).toBe('true')
  })

  it('Should have air conditioned equipment filter existing and not checked', () => {
    expect(wrapper.find(selectors.equipmentFilterAirConditioned).attributes()['aria-checked']).toBe('false')
    wrapper.find(selectors.equipmentFilterAirConditioned).trigger('click')
    expect(wrapper.find(selectors.equipmentFilterAirConditioned).attributes()['aria-checked']).toBe('true')
  })

  it('Should have whiteboard equipment filter existing and not checked', () => {
    expect(wrapper.find(selectors.equipmentFilterWhiteboard).attributes()['aria-checked']).toBe('false')
    wrapper.find(selectors.equipmentFilterWhiteboard).trigger('click')
    expect(wrapper.find(selectors.equipmentFilterWhiteboard).attributes()['aria-checked']).toBe('true')
  })
})
