import Component from '@/generators/component.generator'
import meta from './meta.json'

// Component name
const name = 'filters'

// Component declaration
const component = new Component(name)

// Data
const ModelObject = {
  meta
}

component
  .setModel(ModelObject)

export default component
