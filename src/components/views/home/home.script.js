import {
  forEach as _forEach,
  assign as _assign,
  filter as _filter
} from 'lodash-es'
import meta from './meta.json'
import filtersMeta from '@/components/standalone/filters/meta.json'
import UserModel from '@/models/user/user.model'
import filtersService from '@/services/filters/filters.service'
import RoomModel from '@/models/room/room.model'
import Component from '@/generators/component.generator'
import Collection from '@/generators/collection.generator'
import MeetingRoom from '@/components/standalone/room/Room.vue'
import Timeslots from '@/components/standalone/timeslots/Timeslots.vue'
import Filters from '@/components/standalone/filters/Filters.vue'
import Reporting from '@/components/standalone/reporting/Reporting.vue'

// Component name
const name = 'home'

// Component declaration
const component = new Component(name)

// Sub components
const subComponents = {
  MeetingRoom,
  Timeslots,
  Reporting,
  Filters
}

// Fake user (Done normaly on user login)
const user = new UserModel('me', 'anthony', '123456')
user
  .loadSubModels({ names: 'profile' })
  .then(() => {
    user
      .loadSubModels({
        names: 'timeslots',
        url: `timeslots/user_${user.profile.id}_timeslots`
      })
  })

// Generate collection of meeting room
const meetingRooms = new Collection('meetingRooms', RoomModel)
const buildTarget = process.env.VUE_APP_BUILD_TARGET
const pepsiRooms = 'p01, p02, p03, p04, p05, p06, p07, p08, p09, p10'
const cocaRooms = 'c01, c02, c03, c04, c05, c06, c07, c08, c09, c10'
if (buildTarget === 'coca') {
  meetingRooms .addModelIds(cocaRooms).addModelIds(pepsiRooms)
} else {
  meetingRooms.addModelIds(pepsiRooms).addModelIds(cocaRooms)
}
meetingRooms.loadSubModels({ names:'identity, timeslots' })

/**
 * Open time slots panel for the selected meeting room
 * @param  {String} roomId (meeting room model id)
 */
function openRoomTimeslots (roomId) {
  const room = meetingRooms.getModel(roomId)
  const { label, id } = room.getSubModel('identity')

  this.meta.states.activeRoomLabel = label
  this.meta.states.userBooking = false
  this.meta.states.isReporting = false
  this.meta.states.activeRoomId = id
  this.meta.states.rightDrawer = true
}

/**
 * Open user time slots
 */
function openUserBooking () {
  this.meta.states.activeRoomLabel = null
  this.meta.states.activeRoomId = null
  this.meta.states.isReporting = false
  this.meta.states.rightDrawer = true
  this.meta.states.userBooking = true
}

/**
 * Open reporting panel for the selected meeting room
 * @param  {String} roomId (meeting room model id)
 */
function openReporting (roomId) {
  const room = meetingRooms.getModel(roomId)
  const { label, id } = room.getSubModel('identity')

  this.meta.states.activeRoomLabel = label
  this.meta.states.isReporting = true
  this.meta.states.activeRoomId = id
  this.meta.states.rightDrawer = true
}

/**
 * [CALLBACK METHOD] Filters updated from filters component
 * @param  {Object} data (Filters states)
 */
function updateFilters (data) {
  _assign(this.meta.filters, data)
}

/**
 * [COMPUTED METHOD] Filter the collection depending filter component
 *
 * @return {Array} filteredValues
 */
function filteredCollection () {
  let filteredValues = this.collection

  if (Boolean(this.meta.filters.minimumSeats)) {
    filteredValues = _filter(filteredValues, room => {
      return room.identity.specifications.seats.number >= this.meta.filters.minimumSeats
    })
  }

  _forEach(this.meta.filters.requiredEquipments, equipment => {
    if (equipment.checked) {
      filteredValues = filtersService.filterEquipment(filteredValues, equipment.id)
    }
  })

  if (Boolean(this.meta.filters.startingHour) && Boolean(this.meta.filters.duration)) {
    filteredValues = _filter(filteredValues, room => {
      const startingIndex = room.timeslots.findIndex(item => {
        return item.timeslot.start ===  this.meta.filters.startingHour.hour &&
               item.timeslot.meridiem === this.meta.filters.startingHour.meridiem &&
               !item.booked
      })
      const consecutiveHourAvailable = filtersService.consecutiveTimeslotCounter(room.timeslots, startingIndex)
      return this.meta.filters.duration <= consecutiveHourAvailable
    })
  }

  if (Boolean(this.meta.filters.duration) && !Boolean(this.meta.filters.startingHour)) {
    filteredValues = _filter(filteredValues, room => {
      const consecutiveHourAvailable = filtersService.consecutiveTimeslotCounter(room.timeslots)
      return this.meta.filters.duration <= consecutiveHourAvailable
    })
  }

  return filteredValues
}

/**
 * Update room status using models because
 * it's a mock for now
 * @param  {String} roomId (Room id)
 */
function updateRoomStatus (roomId) {
  const room = meetingRooms.getModel(roomId)
  room.updateIdentityStatus()
}

// Component model
meta.filters = filtersMeta.states
const Model = {
  collection: meetingRooms.getCollectionArray(),
  meta,
  user
}

component
  .setSubComponents(subComponents)
  .setComputed(filteredCollection)
  .setMethods(updateRoomStatus)
  .setMethods(openUserBooking)
  .setMethods(openRoomTimeslots)
  .setMethods(openReporting)
  .setMethods(updateFilters)
  .setModel(Model)

export default component
