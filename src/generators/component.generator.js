import {
  isPlainObject as _isPlainObject,
  isFunction as _isFunction,
  isArray as _isArray,
  assign as _assign
} from 'lodash-es'

/**
 * Set either methods or computed properties
 * @param {Object|Function} methods   (List of methods or single method)
 * @param {String} type               (Type of methods)
 */
function _setMethods (methods, type = 'methods') {
  const object = {}

  if (type !== 'methods' && type !== 'computed') {
    throw new Error(`${type} value for methods arguments not supported`)
  }

  if (!_isPlainObject(methods) && !_isFunction(methods)) {
    throw new Error('`methods` argument must be either a function or an object')
  }

  if (_isFunction(methods)) {
    object[methods.name] = methods
    _assign(this[type], object)
  }

  if (_isPlainObject(methods)) {
    _assign(this[type], methods)
  }

  return this
}

class Generator {
  constructor (name) {
    this.name = name
    this.model = {}
    this.props = []
    this.methods = {}
    this.filters = {}
    this.computed = {}
    this.components = {}
  }

  /**
   * Set methods
   * @param {Object|Function} methods   (List of methods or single method)
   */
  setMethods (methods) {
    _setMethods.call(this, methods)

    return this
  }

  /**
   * Set computed function
   * @param {Function} action
   */
  setComputed (methods) {
    _setMethods.call(this, methods, 'computed')

    return this
  }

  /**
   * Set properties from parent component
   * @param {Array/Object} properties (props)
   *
   * @return {Object} this
   */
  setBoundProperties (properties) {
    if (_isArray(properties)) {
      this.props = [...this.props, ...properties]

      return this
    }

    this.props = properties

    return this
  }

  /**
   * Add components references to the view model
   * @param {object} components (list of view models)
   *
   * @return {object} this
   */
  setSubComponents (components) {
    _assign(this.components, components)

    return this
  }

  /**
   * Set data model of the component
   * @param {Object} data (Model)
   */
  setModel (data) {
    this.data = function () {
      return _assign(this.model, data)
    }

    return this
  }

  /**
   * Set function to call before mount
   * @param {Function} func
   */
  setBeforeMount (func) {
    this.beforeMount = func

    return this
  }

  /**
   * Set function to call on mount
   * @param {Function} func
   */
  setMounted (func) {
    this.mounted = func

    return this
  }
}

export default Generator
