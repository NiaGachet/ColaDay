import {
  isString as _isString,
  includes as _includes,
  isArray as _isArray
} from 'lodash-es'
import utils from '@/services/utils/utils.service'

class Model {
  constructor (id) {
    this.id = id || null
    this._submodelConfig = {}
    this.status = {
      loading: false,
      generatedSubModels: [],
      loadedSubModels: []
    }
  }

  /**
   * Populate sub models
   * @param  {String}  name   (name of the submodel && associated mapping file)
   * @param  {Object}  data   (row data for the sub model)
   * @param  {Boolean} update (update sub model)
   */
  _populateSubModel (name, data, update) {
    if (this._hasSubModelLoaded(name) && !update) {
      console.warn(`Model: submodel to populate < ${name} > is already loaded, you have to update`)

      return
    }

    if (this._submodelConfig[name].isCollection) {
      this[name].length = 0
      if (!_isArray(data)) {
        console.error(`Model < ${name} > has wrong data type, must be an array`)
        return
      }
      for (const item of data) {
        const Model = this._submodelConfig[name].mapping
        this[name].push(new Model(item))
      }

      this.status.loadedSubModels.push(name)
      return
    }

    this[name].set(data)
    this.status.loadedSubModels.push(name)
  }

  /**
   * Add sub model to the current model
   * @param {String/Array} name (name of the mapping && submodel)
   */
  _generateSubModels (name) {
    if (this[name]) {
      return
    }
    if (!this._submodelConfig[name].mapping) {
      throw new Error(`Model: submodel to add < ${name} > doesn't have referenced mapping`)
    }
    if (this._submodelConfig[name].isCollection) {
      this[name] = []
      this.status.generatedSubModels.push(name)

      return
    }

    const Model = this._submodelConfig[name].mapping
    this[name] = new Model()
    this.status.generatedSubModels.push(name)
  }

  /**
   * Used to know if the specified submodel is loaded
   * @param  {String}  name (submodel name)
   *
   * @return {Boolean}
   */
  _hasSubModelLoaded (name) {
    return _includes(this.status.loadedSubModels, name)
  }

  /**
   * Used to know if the specified submodel is generated
   * @param  {String}  name (submodel name)
   *
   * @return {Boolean}
   */
  _hasSubModelGenerated (name) {
    return _includes(this.status.generatedSubModels, name)
  }

  /**
   * Set sub model can be called to populate block models manually
   * @param {String}       options.name   (mapping / submodel name)
   * @param {Object/Array} options.data   (data, or collection of data)
   * @param {Boolean}      options.update (update an existing submodel)
   *
   * return {Model}
   */
  setSubModel ({name, data, update}) {
    if (!_isString(name)) {
      throw new Error(`Model: setSubModel < ${name} > must be a String`)
    }

    this._generateSubModels(name)

    if (!data) {
      console.warn(`Model: submodel < ${name} > doesn't have associated data. You can use load() if a resource url exist`)

      return this
    }

    this._populateSubModel(name, data, update)

    return this
  }

  /**
   * Get an existing sub model
   * @param  {String} name (submodel name)
   *
   * @return {Object}      (submodel)
   */
  getSubModel (name) {
    if (!this._hasSubModelLoaded(name)) {
      console.warn(`Model: requested submodel < ${name} > doesn't exist. Available submodel are:`)
      console.table(this.status.loadedSubModels)

      return
    }

    return this[name]
  }

  /**
   * Loading resource
   * @param  {[type]} names  [description]
   * @param  {[type]} update [description]
   *
   * @return {Promise}
   */
  async loadSubModels ({names, url, update}) {
    this.status.loading = true
    const subModelsToLoad = utils.stringToArray(names)

    for (let name of subModelsToLoad) {
      this._generateSubModels(name)

      const resourceUrl = this._submodelConfig[name].url || url
      if (!resourceUrl) {
        console.warn(`Model: submodel to load < ${name} > doesn't have referenced ressource url`)
      }

      if (this._submodelConfig[name].isMock) {
        const data = await import(/* webpackMode: "eager" */`@/assets/mocks/${resourceUrl}.json`)
        this._populateSubModel(name, data.response, update)
      }
    }

    this.status.loading = false
  }
}

export default Model
