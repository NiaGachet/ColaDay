import utils from '@/services/utils/utils.service'

class Collection {
  constructor (id, Model, url) {
    this.id = id
    this._config = {
      Model,
      url
    }
    this.status = {
      loading: false
    }
    this.collection = new Map()
  }

  /**
   * Add a model to collection
   * @param {Model} model
   */
  addModel (model) {
    this.collection.set(model.id, model)

    return this
  }

  /**
   * Check if the model is in the collection
   * @param  {Strong}  id (model id)
   *
   * @return {Boolean}
   */
  hasModel (id) {
    return this.collection.has(id)
  }

  /**
   * Get model from collection
   * @param  {String} id (Model id)
   *
   * @return {Model}
   */
  getModel (id) {
    return this.collection.get(id)
  }

  /**
   * Remove a model from the collection
   * @param  {String} id
   */
  removeModel (id) {
    this.collection.delete(id)

    return this
  }

  /**
   * Add model to collection
   * @param {Strong} modelIds (Id of a model)
   */
  addModelIds (modelIds) {
    const ids = utils.stringToArray(modelIds)

    for (let id of ids) {
      if (this.hasModel(id)) {
        continue
      }
      const item = new this._config.Model(id)
      this.addModel(item)
    }

    return this
  }

  /**
   * Load submodels
   * @param  {String/Array} submodelNames (list of submodels to load)
   */
  loadSubModels (submodelNames, update) {
    this.status.loading = true

    if (!this._config.url) {
      this.collection.forEach(model => model.loadSubModels(submodelNames, update))
    }

    this.status.loading = false
  }

  /**
   * Get collection array
   *
   * @return {Array} (list of models)
   */
  getCollectionArray () {
    return Array.from(this.collection.values())
  }

  /**
   * Clear all the collection
   */
  clear () {
    this.collection.clear()

    return this
  }
}

export default Collection
