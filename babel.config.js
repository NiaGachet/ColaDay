module.exports = {
  presets: [
    '@vue/app'
  ],
  "env": {
    "dev": {
      "plugins": [
        [
        "transform-imports",
          {
            "vuetify": {
              "transform": "vuetify/es5/components/${member}",
              "preventFullImport": true
            }
          }
        ]
      ]
    },
  }
}
