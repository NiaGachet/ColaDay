# BookMeRoo for Cola Day !

> A meeting room booking system

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload with pepsi build
npm run serve:pepsi

# serve with hot reload with pepsi build
npm run serve:coca

# build for production with minification
npm run build

# run unit tests
npm run test:unit

```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader). For more information about the product, please [read the documentation](https://gitlab.com/NiaGachet/ColaDay/wikis/home)
