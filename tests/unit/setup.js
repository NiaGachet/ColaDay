// Remove console.warn from vuetify
global.console.warn = jest.fn()
// Remove console.error from vue
global.console.error = jest.fn()
